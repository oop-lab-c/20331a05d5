//Java Program to demonstrate overiding of methods/functions
import java.util.*;
class Parent //Parent class
{
 void fun()
 {
     System.out.println("Parent class is invoked");
 }
}
class child extends Parent  //child class is derived from Parent class
{
    void fun()
    {
        System.out.println("child class is invoked");
    }
}
public class MethodORJava 
{
  public static void main(String[] args)
  {
      child obj = new child(); // creating object for child class
      obj.fun();  // displays the contents in the child i.e by overriding the method present in parent class
  }    
}
