class Publicinheritance
{  
    int x=77;  
} 
class Privateinheritance 
{
   private int numl;
   public void setpri(int n) 
  { 
     numl = n;
  } 
  public int getpri() 
  { 
     return numl;
  } 
} 
 class Publicinheritance
{
  protected int num2;
  public void setpro(int n) 
  {
    num2 = n;
  } 
  public int getpro() 
  { 
    return num2;
  } 
} 
class Derivedpri extends  Privateinheritance
{ 
    public void mul() 
    {
        int num = getpri();
        System.out.println("product = " + (num *num)); 
    }
} 
class Derivedpro extends  Protectedinheritance
{ 
    public void add() 
    {
        int num = getpro();
        System.out.println("sum = " + (num2 +num)); 
    }
}   
class Derivedpub extends Publicinheritance
{  
    int y = 660;  
}
class PPPInheriJava
{
  public static void main(String[] args)
  {
    Derivedpri obj = new Derivedpri();
    obj.setpri(10);
    obj.mul();
    Derivedpro a = new Derivedpro();
    a.setpro(10);
    a.add();
    Derivedpub b = new Derivedpub();
    System.out.println(b.x);
  }
}
/*
observation
   we can access the private members using get and set methods
   public static void main(String[] args) 
       { 
           Derivedpriv d = new Derivedpriv(); 
           d.setvalue(20); //to set private member numl 
           d.num2 = 10; 
           d.mul(); 
       } */

