//Java program to demonstrate default and parameterized constructor and destructor
import java.util.*;
class Student
{
    String collegeName,fName;
    int collegeCode;
    double sPercentage;
    //Assigning values using Default constructor
    Student()
    {
        collegeName="MVGR";
        collegeCode=33;
    }
    //Assigning values using parameterized constructor
    Student(String fullName,double semPercentage)
    {
        fName=fullName;
        sPercentage=semPercentage;
    }
    //function to display the Student details of default constructor
    void display1()
    {
        System.out.println("College Name: "+collegeName);
        System.out.println("College code: "+collegeCode);
    }
    //function to display the Student details of parameterized constructor
    void display2()
    {
        System.out.println("Name: "+fName);
        System.out.println("Sem Percentage: "+sPercentage);
    }
    //destructor-clean up process before the object is garbage collected
    protected void finalize()
    {
        System.out.println("Object destroyed");
    };
}
class ParamConstJava
{
    public static void main(String[] args)
    {
    String collegeName;
    int collegeCode;
    //Reading the user input from the console
    System.out.println("Enter your Full Name : ");
    Scanner input=new Scanner(System.in);
    String fName=input.nextLine();
    System.out.println("Enter your Sem Percentage : ");
    double sPercentage=input.nextDouble();
    Student obj1=new Student();//object created and default constructor called
    Student obj2=new Student(fName,sPercentage);//object created and parameterized constructor called
    obj1.display1();
    obj2.display2();
    obj1=obj2=null;
    System.gc();
    }
}  
