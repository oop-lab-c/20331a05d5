//Java program to display Hello 'Username'', where 'Username' will be given by the user
import java.util.*;
public class HelloUserJava
{
    public static void main(String[] args)
    {
        System.out.println("Enter your name:");
        Scanner input=new Scanner(System.in);//creating an object to the Scanner class
        //Reading user input from the console
        String Username=input.nextLine();//calling nextline method using object
        System.out.println("Hello "+Username);
    }
}
