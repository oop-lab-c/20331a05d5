
import java.util.*;
class CalcJava
{
    public static void main(String[] args)
    {
        Scanner input=new Scanner(System.in);
        System.out.println("enter operand-1:");
        int a=input.nextInt(); 
        System.out.println("enter operand-2:");
        int b=input.nextInt();
        System.out.println("Enter any operator of your choice:(+,-,*,/,%):");
        char op=input.next().charAt(0);
        if(op=='+')
        {
            System.out.println("Result is: "+(a+b));
        }
        else if(op=='-')
        {
            System.out.println("Result is: "+(a-b));
        }
        else if(op=='*')
        {
            System.out.println("Result is: "+(a*b));
        }
        else if(op=='/')
        {
            if(b==0)
            System.out.println("denominator should not be zero");
            else
            System.out.println("Quotient is: "+((float)a/b));
        }
        else if(op=='%')
        {
            if(b==0)
            System.out.println("denominator should not be zero");
            else
            System.out.println("Remainder is: "+(a%b));
        }
        else
        {
            System.out.println("Invalid operator");
        }
    }
}