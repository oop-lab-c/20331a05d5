//Java function to check whether a number is even or odd.
import java.util.*;
class EvenOddJava
{
    static void oddorevenCheck(int n)
    {
        if(n%2==0)
            System.out.println(n+" is even");
        else
            System.out.println(n+" is odd");
    }
    public static void main(String[] args)
    {
    System.out.println("Enter any number:");
    Scanner input=new Scanner(System.in);
    int n=input.nextInt();
    oddorevenCheck(n);
    }
}